<?php

    require_once('animal.php');
    require_once('ape.php');
    require_once('frog.php');

    $object = new animal ("shaun");
    echo "Name: " . $object->name ."<br>";
    echo "leg : " . $object->legs . "<br>";
    echo "cold-blooded: ". $object ->cold_blooded . "<br>";
    echo "<br>";

    $kodok = new frog("Buduk");
    echo "Name: " . $kodok->name ."<br>";
    echo "leg : " . $kodok->legs . "<br>";
    echo "cold-blooded: ". $kodok ->cold_blooded . "<br>";
    $kodok->jump();
    echo "<br>";

    $sungokong = new Ape("kera sakti");
    echo "Name: " . $sungokong->name ."<br>";
    echo "leg : " . $sungokong->legs . "<br>";
    echo "cold-blooded: ". $sungokong ->cold_blooded . "<br>";
    $sungokong->yell();
    echo "<br>";

    



?>